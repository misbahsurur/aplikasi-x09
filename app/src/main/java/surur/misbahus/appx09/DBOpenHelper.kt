package surur.misbahus.appx09

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper(context : Context) : SQLiteOpenHelper (context, DB_Name,null, DB_Ver){
    companion object{
        val DB_Name = "AppX09"
        val DB_Ver = 1
    }
    override fun onCreate(db: SQLiteDatabase?) {
        val tMusic = "create table music(id_music text, id_cover text, music_title text)"
        val insMusic = "insert into music values('0x7f0c0000', '0x7f06005f', 'Powfu - Death Bed'), ('0x7f0c0001', '0x7f060060', 'Rainych - Say So'), ('0x7f0c0002', '0x7f060061', 'TK - Unravel')"
        db?.execSQL(tMusic)
        db?.execSQL(insMusic)
    }
    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }
}