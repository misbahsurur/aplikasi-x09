package surur.misbahus.appx09

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.media.MediaPlayer
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), View.OnClickListener, SeekBar.OnSeekBarChangeListener {

   // val daftarLagu = intArrayOf(R.raw.music_1, R.raw.music_2, R.raw.music_3)
   // val daftarCover = intArrayOf(R.drawable.cover_1, R.drawable.cover_2, R.drawable.cover_3)
   // val daftarJudul = arrayOf("Death Bed", "Say So", "Unravel")
    lateinit var db : SQLiteDatabase
    lateinit var adapter : ListAdapter
    lateinit var  v : View
    var posLaguSkrg = 0
    var posJudulSkrg = ""
    var posCoverSkrg = 0
    var handler = Handler()
    var id_mus : Int = 0x7f0c0000
    var id_cov : Int = 0x7f06005f
    var id_jud : String = "Powfu - Death Bed"
    var pl_mus : Int = id_mus
    var pl_cov : Int = id_cov
    var pl_jud : String = id_jud
    var max : String = ""
    lateinit var mediaPlayer : MediaPlayer
    lateinit var mediaController: MediaController

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
        seekBar?.progress?.let { mediaPlayer.seekTo(it) }
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnPlay ->{
                play(posLaguSkrg)
                audioPlay()
            }
            R.id.btnNext ->{
                audioNext()
            }
            R.id.btnPrev ->{
                audioPrev()
            }
            R.id.btnStop ->{
                audioStop()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        db = DBOpenHelper(this).writableDatabase
        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        seekSong.max=100
        seekSong.progress = 0
        seekSong.setOnSeekBarChangeListener(this)
        btnNext.setOnClickListener(this)
        btnPrev.setOnClickListener(this)
        btnStop.setOnClickListener(this)
        btnPlay.setOnClickListener(this)
        ls_musik.setOnItemClickListener(itemClick)
    }

    override fun onStart() {
        super.onStart()
        showdatamusik()
    }

    val itemClick = AdapterView.OnItemClickListener { parent, view, position, id ->
        posLaguSkrg=position
        audioStop()
        play(position)
    }

    fun showdatamusik(){
        var sql = "select id_music as _id, id_cover, music_title from music"
        //max = "select count(id_music) as id from music"
        val c : Cursor = db.rawQuery(sql,null)
        adapter = SimpleCursorAdapter(this,R.layout.list_musik,c, arrayOf("_id","id_cover","music_title"),
            intArrayOf(R.id.tid_music,R.id.tid_cover,R.id.tid_judul),
            CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER)
        ls_musik.adapter = adapter
    }

    fun milliSecondToString(ms : Int):String{
        var detik = TimeUnit.MILLISECONDS.toSeconds(ms.toLong())
        val menit = TimeUnit.SECONDS.toMinutes(detik)
        detik = detik % 60
        return "$menit : $detik"
    }

    fun audioPlay(){
        audioStop()
        mediaPlayer.start()
        var updateSeekBarThread = UpdateSeekBarProgressThread()
        handler.postDelayed(updateSeekBarThread,50)
    }

    fun audioNext(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
        if(posLaguSkrg<(ls_musik.adapter.count-1)){
            posLaguSkrg++
        }else{
            posLaguSkrg = 0
        }
        play(posLaguSkrg)
        audioPlay()
    }

    fun audioPrev(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
        if(posLaguSkrg>0){
            posLaguSkrg--
        }else{
            posLaguSkrg = ls_musik.adapter.count-1
        }
        play(posLaguSkrg)
        audioPlay()
    }

    fun audioStop(){
        if(mediaPlayer.isPlaying) mediaPlayer.stop()
    }

    fun play(pos : Int){
        val cu : Cursor = ls_musik.adapter.getItem(pos) as Cursor
        pl_mus = cu.getInt(cu.getColumnIndex("_id"))
        pl_cov = cu.getInt(cu.getColumnIndex("id_cover"))
        pl_jud = cu.getString(cu.getColumnIndex("music_title"))
        mediaPlayer = MediaPlayer.create(this, pl_mus)
        seekSong.max = mediaPlayer.duration
        txMaxTime.setText(milliSecondToString(seekSong.max))
        txCrTime.setText(milliSecondToString(mediaPlayer.currentPosition))
        seekSong.progress = mediaPlayer.currentPosition
        imV.setImageResource(pl_cov)
        txJudulLagu.setText(pl_jud)
    }

    inner class UpdateSeekBarProgressThread : Runnable{
        override fun run() {
            var currTime = mediaPlayer.currentPosition
            txCrTime.setText(milliSecondToString(currTime))
            seekSong.progress=currTime
            if(currTime != mediaPlayer.duration) handler.postDelayed(this,50)
        }
    }

}
